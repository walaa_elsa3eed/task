<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class HotelsTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testExample()
    {
        $response = $this->json('POST', 'api/hotels',
            [
                'from_date' => '2020-02-5',
                'to_date'=>'2020-02-09',
                'city_code'=>'CA',
                'no_adults'=>4
            ]);

        $response->assertStatus(200);
    }
}
