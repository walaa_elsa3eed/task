<?php

namespace App\Transformers;

use Illuminate\Support\Facades\Validator;
use App\Validations\HotelsValidationContract;
use Illuminate\Validation\ValidationException;

class LaravelValidation implements HotelsValidationContract
{
    /**
     * Validate user inputs
     *
     * @param  array  $data
     * @return bool
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function validate(array $data): bool
    {
        $rules = [
            'dateFrom' => ['required_if:from_date,','date' , 'before:dateTo'],
            'dateTo' => ['required_if:to_date,','date','after:dateFrom'],
            'city' => ['required_if:city_code,','string'],
            'adults' => ['required_if:no_adults,','integer'],
            'from_date' => ['required_if:dateFrom,','date','before:to_date'],
            'to_date' => ['required_if:dateTo,','date','after:from_date'],
            'city_code' => ['required_if:city,','string'],
            'no_adults' => ['required_if:adults,','integer'],
        ];

        $validator = Validator::make($data, $rules);

        if ($validator->fails()) {
            throw new ValidationException($validator);
        }

        return true;
    }
}
