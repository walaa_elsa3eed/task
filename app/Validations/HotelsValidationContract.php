<?php


namespace App\Validations;


interface HotelsValidationContract
{
    /**
     * Validate user inputs
     *
     * @param  array  $data
     * @return bool
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function validate(array $data): bool;

}
