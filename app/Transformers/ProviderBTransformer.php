<?php


namespace App\Transformers;
use League\Fractal\TransformerAbstract;


class ProviderBTransformer extends TransformerAbstract
{
    /**
     * Transform the given data
     *
     * @param  array  $hotel
     * @return array
     */
    public function transform(array $hotel)
    {
        $data = [
            'hotelName'         => $hotel['hotelName'] ?? null,
            'Rate'              => str_repeat('*',$hotel['Rate']) ?? null,
            'Price'             => $hotel['Price'] ?? null,
            'discount'          => $hotel['discount'] ?? null,
            'amenities'         => json_encode($hotel['amenities'],true) ?? null,
        ];

        return array_filter($data, function ($item) {
            return !is_null($item);
        });
    }
}
