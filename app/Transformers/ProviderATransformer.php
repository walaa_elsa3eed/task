<?php


namespace App\Transformers;
use League\Fractal\TransformerAbstract;


class ProviderATransformer extends TransformerAbstract
{
    /**
     * Transform the given data
     *
     * @param  array  $hotel
     * @return array
     */
    public function transform(array $hotel)
    {

        $data = [
            'Hotel'         => $hotel['hotelName'] ?? null,
            'Rate'              => $hotel['Rate'] ?? null,
            'Fare'             => $hotel['Price'] ?? null,
            'roomAmenities'         =>  implode (", ", $hotel['amenities'])?? null,
        ];

        return array_filter($data, function ($item) {
            return !is_null($item);
        });
    }
}
