<?php

namespace App\Providers;

use App\Repositories\HotelsRepo;
use App\Repositories\HotelsRepoContract;
use App\Services\HotelsService;
use App\Services\HotelsServiceContract;
use App\Transformers\LaravelValidation;
use App\Validations\HotelsValidationContract;
use Illuminate\Support\ServiceProvider;

class HotelsServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //Hotels Services
        $this->app->bind(HotelsServiceContract::class, HotelsService::class);
        $this->app->bind(HotelsRepoContract::class, HotelsRepo::class);
        $this->app->bind(HotelsValidationContract::class, LaravelValidation::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
