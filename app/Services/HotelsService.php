<?php


namespace App\Services;
use App\Services\HotelsServiceContract;
use App\Repositories\HotelsRepoContract;
use App\Transformers\ProviderATransformer;
use App\Transformers\ProviderBTransformer;
use App\Validations\HotelsValidationContract;
use Spatie\Fractal\FractalFacade;

class HotelsService implements HotelsServiceContract
{
    /**
     * The repository instance.
     *
     * @var App\Repositories\HotelsRepoContract
     */
    protected $repository;


    /**
     * The validation instance.
     *
     * @var App\Validations\HotelsValidationContract
     */
    protected $validator;



    /**
     * @param HotelsRepoContract $repository
     * @param HotelsValidationContract $validator
     */
    public function __construct(
        HotelsRepoContract $repository,
        HotelsValidationContract $validator
    )
    {
        $this->repository = $repository;
        $this->validator = $validator;
    }


    /**
     * Validate user inputs.
     * @param array $data
     * @return bool
     */
    public function validator(array $data): bool
    {
        return $this->validator->validate($data);
    }



    /**
     * Get all the records from the data.
     *
     * @param array $search_keys
     *
     * @return  string  The returned string contains JSON
     */
    public function SearchAllHotels(array $search_keys): string
    {
        dd('test');
        $from_date = $search_keys['from_date'] ?? $search_keys['dateFrom'];
        $to_date = $search_keys['to_date'] ?? $search_keys['dateTo'];
        $city_code = $search_keys['city_code'] ?? $search_keys['city'];
        $adults = $search_keys['no_adults'] ?? $search_keys['adults'];

        $data = $this->repository->SearchAllHotels($from_date,$to_date,$city_code,$adults);

        $providerAkeys = ['dateFrom','dateTo','city','adults'];
        $providerBkeys = ['from_date','to_date','city_code','no_adults'];


        // For testing ammend not last commit 
        if (array_keys($search_keys) == $providerAkeys)
        {
            return FractalFacade::collection($data)
                ->transformWith(new ProviderATransformer())
                ->toJson();
        }
        else if (array_keys($search_keys) == $providerBkeys)
        {
            return FractalFacade::collection($data)
                ->transformWith(new ProviderBTransformer())
                ->toJson();
        }
    }
}
