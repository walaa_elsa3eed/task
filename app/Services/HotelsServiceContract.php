<?php


namespace App\Services;


interface HotelsServiceContract
{

    /**
     * Get all the records from the data
     *
     * @param array $search_keys
     *
     * @return  string  The returned string contains JSON
     */
    function SearchAllHotels(array $search_keys): string;

    /**
     * Validate user inputs.
     *
     * @param array $data
     *
     * @return bool
     */
    public function validator(array $data): bool;

}
