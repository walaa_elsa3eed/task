<?php


namespace App\Repositories;


interface HotelsRepoContract
{
    /**
     * Find a records for the given data.
     *
     * @param string $from_date
     * @param string $to_date
     * @param string $city_code
     * @param int $adults
     *
     * @return array
     *
     */

    public function SearchAllHotels($from_date , $to_date , $city_code, $adults): array;
}
