<?php


namespace App\Repositories;

use App\Repositories\HotelsRepoContract;

class HotelsRepo implements HotelsRepoContract
{

    public $hotels =
        [
            [
                "hotelName"=>"Kempinski Nile Hotel Garden City",
                "Rate"=> 5,
                "Price"=> 9219,
                "from_date" => "2020-01-10",
                "to_date" => "2020-01-13",
                "city_code" => "CA",
                "adults" => 2,
                "amenities"=>['Safety Deposit Box,Meeting Rooms,Air Conditioning,Business Center,Concierge,Elevators,Foreign Currency Exchange,Sauna']
            ],
            [
            "hotelName"=> "Le Sphinx Hotel",
                "Rate"=> 5,
                "Price"=> 9234,
                "discount"=> 10,
                "from_date" => "2020-01-5",
                "to_date" => "2020-01-7",
                "city_code" => "AL",
                "adults" => 1,
                "amenities"=> ['Air Conditioning,Business Center,Concierge,Elevators,Foreign Currency Exchange,Sauna']
            ],
            [
            "hotelName"=> "Queen's Hotel",
                "Rate"=> 3,
                "Price"=> 2345,
                "from_date" => "2020-02-01",
                "to_date" => "2020-02-03",
                "city_code" => "MA",
                "adults" => 3,
                "amenities"=> ['Restaurant,Laundry Service,Tour Desk,Multilingual Staff,Doctor On Call']
            ],
            [
            "hotelName"=> "Holiday Inn",
                "Rate"=> 4,
                "Price"=> 4450,
                "discount"=> 120,
                "from_date" => "2020-02-5",
                "to_date" => "2020-02-09",
                "city_code" => "CA",
                "adults" => 4,
                "amenities"=> ['Business Center,Concierge,Elevators,Foreign Currency Exchange,Sauna']
            ],
            [
            "hotelName"=> "Al Masah Hotel And Spa",
                "Rate"=> 2,
                "Price"=> 3100,
                "from_date" => "2020-01-8",
                "to_date" => "2020-01-12",
                "city_code" => "MA",
                "adults" => 2,
                "amenities"=> ['Spa,Kids Pool,Gymnasium,Swimming Pool - Outdoor,Restaurant']
            ],
            [
                "hotelName"=> "Dubai International Terminal Hotel",
                "Rate"=> 5,
                "Price"=> 3306,
                "from_date" => "2020-01-20",
                "to_date" => "2020-01-23",
                "city_code" => "AL",
                "adults" => 1,
                "amenities"=> ['Safety Deposit Box,Meeting Rooms,Air Conditioning,Business Center,Concierge,Elevators,Foreign Currency Exchange,Sauna']
            ],
            [
                "hotelName"=> "Media Rotana Dubai",
                "Rate"=> 5,
                "Price"=>1307,
                "from_date" => "2020-01-10",
                "to_date" => "2020-01-13",
                "city_code" => "CA",
                "adults" => 2,
                "amenities"=> ['Air Conditioning,Business Center,Concierge,Elevators,Foreign Currency Exchange,Sauna']
            ],
            [
                "hotelName"=> "Queen's Hotel",
                "Rate"=> 2,
                "Price"=> 881,
                "from_date" => "2020-01-8",
                "to_date" => "2020-01-12",
                "city_code" => "MA",
                "adults" => 2,
                "amenities"=> ['Restaurant,Laundry Service,Tour Desk,Multilingual Staff,Doctor On Call']
            ],
            [
                "hotelName"=> "Icon Hotel Apartments",
                "Rate"=> 3,
                "Price"=> 1048,
                "from_date" => "2020-02-5",
                "to_date" => "2020-02-09",
                "city_code" => "CA",
                "adults" => 4,
                "amenities"=> ['Business Center,Concierge,Elevators,Foreign Currency Exchange,Sauna']
            ],
            [
                "hotelName"=> "Dubai International Terminal Hotel",
                "Rate"=> 3,
                "Price"=> 1114,
                "from_date" => "2020-02-01",
                "to_date" => "2020-02-03",
                "city_code" => "MA",
                "adults" => 3,
                "amenities"=> ['Spa,Kids Pool,Gymnasium,Swimming Pool - Outdoor,Restaurant']
            ]
        ];

    /**
     * Get all records from the data.
     *
     * @param string $from_date
     * @param string $to_date
     * @param string $city_code
     * @param int $adults
     * @return array
     */
    public function SearchAllHotels($from_date , $to_date , $city_code, $adults): array
    {
        dd($this->hotels);
        $hotels = collect($this->hotels);
        $result = $hotels
            ->where('from_date',$from_date)
            ->where('to_date',$to_date)
            ->where('city_code',$city_code)
            ->where('adults',$adults)
            ->sortByDesc('Rate')
            ->all();
        return ($result);
    }

}
