<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\HotelsServiceContract;

class HotelsController extends Controller
{
    /**
     * Hotel service object.
     *
     * @var App\Services\HotelsServiceContract
     */
    protected $service;

    /**
     * Create a new controller instance.
     *
     * @param App\Services\HotelsServiceContract
     * @return void
     */
    public function __construct(
        HotelsServiceContract $service
    )
    {
        $this->service = $service;
    }


    public function SearchHotels(Request $request)
    {
        // Testing
        $search_keys = $request->all();

        //For Ammend commit 
        $this->validator($search_keys);
        $result = json_decode($this->service->SearchAllHotels($search_keys),true);

        return response()->json($result['data'], 200);
    }


    /**
     * Get a validator for an incoming  request.
     *
     * @param array $data
     * @return bool
     */
    protected function validator(array $data)
    {
        return $this->service->validator($data);
    }
}
